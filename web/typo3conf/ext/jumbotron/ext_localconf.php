<?php

defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    'mod.wizards.newContentElement.wizardItems.special {
    elements {
        jumbotron {
            iconIdentifier = content-menu-sitemap
            title = LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.wizard.title
             description = LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.wizard.description
         tt_content_defValues {
                CType = jumbotron
         }
      }
   }
   show := addToList(jumbotron)
}'
);