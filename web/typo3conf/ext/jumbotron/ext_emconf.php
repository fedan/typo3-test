<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Jumbotron',
    'description' => 'The Jumbotron content element',
    'category' => 'be',
    'author' => 'Mykola Fedan',
    'author_company' => 'Special elements Inc.',
    'author_email' => 'mykola.fedan@gmail.com',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Jumbotron\\Jumbotron\\' => 'Classes'
        ],
    ],
];