<?php

defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    [
        'LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.wizard.title',
        'jumbotron',
        'EXT:jumbotron/Resources/Public/Icons/Extension.svg'
    ],
    'CType',
    'jumbotron'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content', ['button_url' => [
    'exclude' => false,
    'label' => 'LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.palette.button_url',
    'config' => [
        'type' => 'input',
        'renderType' => 'inputLink',
        'size' => 30,
        'max' => 255,
        'eval' => 'trim',
        'fieldControl' => [
            'linkPopup' => [
                'options' => [
                    'title' => 'LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.palette.button_url',
                ],
            ],
        ],
        'softref' => 'typolink'
    ]
],
    'button_text' => [
        'exclude' => false,
        'label' => 'LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.palette.button_text',
        'config' => [
            'type' => 'input',
            'size' => 30,
            'max' => 255,
        ]
    ]], TRUE
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content', 'jumbotron_header', 'header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content', 'jumbotron_header', '--linebreak--'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content', 'jumbotron_header', 'subheader;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:subheader_formlabel'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content', 'jumbotron_button_url', 'button_url;LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.palette.button_url'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content', 'jumbotron_button', 'button_text;LLL:EXT:jumbotron/Resources/Private/Language/local.xlf:jumbotron.palette.button_text'
);

$GLOBALS['TCA']['tt_content']['types']['jumbotron'] = [
    'showitem' => '
       -div--;
                   LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
       --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
       --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.header;jumbotron_header,
          bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
       --palette--;LLL:EXT:jumbotron_exe/Resources/Private/Language/local.xlf:jumbotron.palette.button_url;jumbotron_button_url,
       --palette--;LLL:EXT:jumbotron_exe/Resources/Private/Language/local.xlf:jumbotron.palette.button;jumbotron_button,
   ',
    'columnsOverrides' => [
        'bodytext' => [
            'config' => [
                'enableRichtext' => true,
                'richtextConfiguration' => 'default'
            ]
        ]
    ]
];


